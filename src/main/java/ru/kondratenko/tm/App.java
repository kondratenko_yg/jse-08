package ru.kondratenko.tm;

import static ru.kondratenko.tm.constant.TerminalConst.*;

import ru.kondratenko.tm.dao.ProjectDAO;
import ru.kondratenko.tm.dao.TaskDAO;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;

import java.util.Scanner;

public class App {

    private static final ProjectDAO projectDAO = new ProjectDAO();

    private static final TaskDAO taskDAO = new TaskDAO();

    private static final Scanner scanner = new Scanner(System.in);

    static {
        projectDAO.create("project1");
        projectDAO.create("project2");
        taskDAO.create("task1");
        taskDAO.create("task2");
    }

    public static void main(final String[] args) {
        displayWelcome();
        process();
    }

    private static void process() {
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    private static int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION:
                return displayVersion();
            case ABOUT:
                return displayAbout();
            case HELP:
                return displayHelp();
            case EXIT:
                return displayExit();

            case PROJECT_CREATE:
                return createProject();
            case PROJECT_CLEAR:
                return clearProject();
            case PROJECT_LIST:
                return listProject();
            case PROJECT_VIEW_BY_INDEX:
                return viewProjectByIndex();
            case PROJECT_VIEW_BY_ID:
                return viewProjectById();
            case PROJECT_REMOVE_BY_INDEX:
                return removeProjectByIndex();
            case PROJECT_REMOVE_BY_ID:
                return removeProjectById();
            case PROJECT_REMOVE_BY_NAME:
                return removeProjectByName();
            case PROJECT_UPDATE_BY_INDEX:
                return updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:
                return updateProjectById();

            case TASK_CREATE:
                return createTask();
            case TASK_CLEAR:
                return clearTask();
            case TASK_LIST:
                return listTask();
            case TASK_VIEW_BY_INDEX:
                return viewTaskByIndex();
            case TASK_VIEW_BY_ID:
                return viewTaskById();
            case TASK_REMOVE_BY_INDEX:
                return removeTaskByIndex();
            case TASK_REMOVE_BY_ID:
                return removeTaskById();
            case TASK_REMOVE_BY_NAME:
                return removeTaskByName();
            case TASK_UPDATE_BY_INDEX:
                return updateTaskByIndex();
            case TASK_UPDATE_BY_ID:
                return updateTaskById();
            default:
                return displayError();
        }
    }

    //---------PROJECT---------------------------------------------------------
    private static int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT NAME:]");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
        final String description = scanner.nextLine();
        projectDAO.create(name, description);
        System.out.println("[OK]");
        return 0;
    }

    private static int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT INDEX:]");
        final Project project = projectDAO.findByIndex(inputIndexCheckFormat());
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER PROJECT NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
            final String description = scanner.nextLine();
            projectDAO.update(project.getId(), name, description);
            System.out.println("[OK]");
        }
        return 0;
    }

    private static int updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT ID:]");
        final Project project = projectDAO.findById(inputIdCheckFormat());
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER PROJECT NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
            final String description = scanner.nextLine();
            projectDAO.update(project.getId(), name, description);
            System.out.println("[OK]");
        }
        return 0;
    }

    private static int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static int removeProjectByIndex() {
        System.out.println("[CLEAR PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX: ");
        final Project project = projectDAO.removeByIndex(inputIndexCheckFormat());
        if (project == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    private static int removeProjectByName() {
        System.out.println("[CLEAR PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME: ");
        final String name = scanner.nextLine();
        final Project project = projectDAO.removeByName(name);
        if (project == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    private static int removeProjectById() {
        System.out.println("[CLEAR PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID: ");
        final Project project = projectDAO.removeById(inputIdCheckFormat());
        if (project == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    private static void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    private static int viewProjectByIndex() {
        System.out.println("ENTER PROJECT INDEX: ");
        final Project project = projectDAO.findByIndex(inputIndexCheckFormat());
        viewProject(project);
        return 0;
    }

    private static int viewProjectById() {
        System.out.println("ENTER PROJECT ID: ");
        final Project project = projectDAO.findById(inputIdCheckFormat());
        viewProject(project);
        return 0;
    }

    private static int listProject() {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        for (final Project project : projectDAO.findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    //--------------------------------------------------------------------
    //------TASK----------------------------------------------------------
    private static int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[PLEASE, ENTER TASK NAME:]");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
        final String description = scanner.nextLine();
        taskDAO.create(name, description);
        System.out.println("[OK]");
        return 0;
    }

    private static int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEASE, ENTER TASK INDEX:]");
        final Task task = taskDAO.findByIndex(inputIndexCheckFormat());
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER TASK NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
            final String description = scanner.nextLine();
            taskDAO.update(task.getId(), name, description);
            System.out.println("[OK]");
        }
        return 0;
    }

    private static int updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEASE, ENTER TASK ID:]");
        final Task task = taskDAO.findById(inputIdCheckFormat());
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        } else {
            System.out.println("[PLEASE, ENTER TASK NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
            final String description = scanner.nextLine();
            taskDAO.update(task.getId(), name, description);
            System.out.println("[OK]");
            return 0;
        }
    }

    private static int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static int removeTaskByName() {
        System.out.println("[CLEAR TASK BY NAME]");
        System.out.println("ENTER TASK NAME: ");
        final String name = scanner.nextLine();
        final Task task = taskDAO.removeByName(name);
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    private static int removeTaskByIndex() {
        System.out.println("[CLEAR TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX: ");
        final Task task = taskDAO.removeByIndex(inputIndexCheckFormat());
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    private static int removeTaskById() {
        System.out.println("[CLEAR TASK BY ID]");
        System.out.println("ENTER TASK ID: ");
        final Task task = taskDAO.removeById(inputIdCheckFormat());
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    private static void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    private static int viewTaskByIndex() {
        System.out.println("ENTER TASK INDEX: ");
        final Task task = taskDAO.findByIndex(inputIndexCheckFormat());
        viewTask(task);
        return 0;
    }

    private static int viewTaskById() {
        System.out.println("ENTER TASK ID: ");
        final Task task = taskDAO.findById(inputIdCheckFormat());
        viewTask(task);
        return 0;
    }

    private static int listTask() {
        System.out.println("[LIST TASK]");
        int index = 1;
        for (final Task task : taskDAO.findAll()) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    //--------------------------------------------------------------------
    private static int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    private static int displayExit() {
        System.out.println("Terminate console application...");
        return 0;
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static int displayHelp() {
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println();
        System.out.println("project-create - Create project.");
        System.out.println("project-clear - Clear list of projects.");
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-view-by-index - Display project by index.");
        System.out.println("project-view-by-id - Display project by id.");
        System.out.println("project-remove-by-id - Remove project by id.");
        System.out.println("project-remove-by-name - Remove project by name.");
        System.out.println("project-remove-by-index - Remove project by index.");
        System.out.println("project-update-by-index - Update project by index.");
        System.out.println("project-update-by-id - Update project by id.");
        System.out.println();
        System.out.println("task-create - Create task.");
        System.out.println("task-clear - Clear list of tasks.");
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-view-by-index - Display task by index.");
        System.out.println("task-view-by-id - Display task by id.");
        System.out.println("task-remove-by-id - Remove task by id.");
        System.out.println("task-remove-by-name - Remove task by name.");
        System.out.println("task-remove-by-index - Remove task by index.");
        System.out.println("task-update-by-index - Update task by index.");
        System.out.println("task-update-by-id - Update task by id.");
        return 0;
    }

    private static int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    private static int displayAbout() {
        System.out.println("Kondratenko Iulia");
        System.out.println("kondratenko_yg@nlmk.com");
        return 0;
    }

    private static int inputIndexCheckFormat() {
        final int index;
        try {
            index = Integer.parseInt(scanner.nextLine()) - 1;
        } catch (Throwable t) {
            System.out.println("[WRONG FORMAT]");
            return -1;
        }
        return index;
    }

    private static long inputIdCheckFormat() {
        final long id;
        try {
            id = Long.parseLong(scanner.nextLine());
        } catch (Throwable t) {
            System.out.println("[WRONG FORMAT]");
            return 0L;
        }
        return id;
    }

}
